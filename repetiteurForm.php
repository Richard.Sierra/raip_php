<?php
require_once "server/dbConnection.php";

$email = $_REQUEST['email'];
$prenom = $_REQUEST['prenom'];
$nom = $_REQUEST['nom'];
$mot_de_passe = $_REQUEST['mot_de_passe'];
$faculte = $_REQUEST['faculte'];
$niveau_etude = $_REQUEST['niveau_etude'];
$est_un_etudiant = 0;
$est_un_repetiteur = 1;


$stmt = $conn->prepare("INSERT INTO RAIP.utilisateur 
    (email, mot_de_passe, prenom, nom, faculte, niveau_etude, est_un_etudiant, est_un_repetiteur) 
    VALUES (?,?,?,?,?,?,?,?);");
$stmt->bind_param("ssssssii", $email, $mot_de_passe, $prenom, $nom, $faculte, $niveau_etude, $est_un_etudiant, $est_un_repetiteur);
$stmt->execute();
header("location: confirmationCompte.html");
$stmt->close();
$conn->close();
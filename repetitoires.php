<?php
require_once "server/dbConnection.php"
?>

<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;


            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            border-radius: 6px;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border: 2px solid #424242;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.33);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.33);
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.33);
            background-color: #1a1a1a;
            color: #808080;
        }

        table, td, th {
            border: 1px solid #780000;
            padding: 5px;
            color: #999999;

            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            border-radius: 6px;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border: 2px solid #424242;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.33);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.33);
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.33);
            background-color: #1a1a1a;
            color: #808080;
        }

        th {
            text-align: left;
            color: #dbdbdb;
        }

        h2 {
            color: #b6b6b6;

        }
    </style>
</head>
<body>

<?php
$q = intval($_GET['q']);

switch ($q) {
    // Comprendre l'activité de programmation
    case 0:
        $sql = "select repetitoire.nom_repetitoire, repetitoire.date_heure, repetitoire.duree, repetitoire.prix_repetitoire_heure
                from repetitoire
                where (date_heure >= now());";
        $result = mysqli_query($conn, $sql);

        echo "<table>
                <tr>
                    <th>Nom répétitoire</th>
                    <th>Date et heure</th>
                    <th>Durée en heures</th>
                    <th>Prix en CHF/heure</th>
                </tr>";
        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            echo "<td>" . $row['nom_repetitoire'] . "</td>";
            echo "<td>" . $row['date_heure'] . "</td>";
            echo "<td>" . $row['duree'] . "</td>";
            echo "<td>" . $row['prix_repetitoire_heure'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        mysqli_close($conn);
        break;

    // Comprendre l'activité de programmation
    case 1:
        // SQL 1 :
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire, 
                     creer_repetitoire, 
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Comprendre l''activité de programmation');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Comprendre l''activité de programmation';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Comprendre l''activité de programmation'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Les types
    case 2:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire, 
                     creer_repetitoire,
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Les types');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Les types';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Les types'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Les instructions conditionnelles
    case 3:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire,
                     creer_repetitoire, 
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Les instructions conditionnelles');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Les instructions conditionnelles';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Les instructions conditionnelles'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Les instructions itératives
    case 4:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire, 
                     creer_repetitoire, 
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Les instructions itératives');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Les instructions itératives';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Les instructions itératives'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Les tableaux et les listes
    case 5:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire,
                     creer_repetitoire,
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Les tableaux et les listes');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Les tableaux et les listes';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Les tableaux et les listes'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Les méthodes
    case 6:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire,
                     creer_repetitoire,
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Les méthodes');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Les méthodes';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Les méthodes'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // La programmation orientée objet
    case 7:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire,
                     creer_repetitoire,
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'La programmation orientée objet');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'La programmation orientée objet';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'La programmation orientée objet'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Les fichiers
    case 8:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire,
                     creer_repetitoire,
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Les fichiers');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Les fichiers';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Les fichiers'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Abstraction procédurale
    case 9:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire,
                     creer_repetitoire,
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Abstraction procédurale');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Abstraction procédurale';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Abstraction procédurale'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Tri des séquences d'objets
    case 10:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire,
                     creer_repetitoire,
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Tri des séquences d''objets');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Tri des séquences d''objets';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Tri des séquences d''objets'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Ensembles et Dictionnaires
    case 11:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire,
                     creer_repetitoire,
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Ensembles et Dictionnaires');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Ensembles et Dictionnaires';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Ensembles et Dictionnaires'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Implémentation des collections
    case 12:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire,
                     creer_repetitoire,
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Implémentation des collections');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Implémentation des collections';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Implémentation des collections'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Récursivité
    case 13:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire,
                     creer_repetitoire,
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Récursivité');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Récursivité';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Récursivité'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Arbre
    case 14:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire,
                     creer_repetitoire,
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Arbres');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Arbres';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Arbres'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Graphe
    case 15:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire,
                     creer_repetitoire,
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Graphes');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Graphes';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Graphes'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    // Algorithmes et programmes
    case 16:
        $sql1 = "select distinct sujet.nom_sujet
                from repetitoire,
                     creer_repetitoire,
                     sujet
                where (repetitoire.ID_repetitoire = creer_repetitoire.ID_repetitoire)
                  and (sujet.ID_sujet = creer_repetitoire.ID_sujet)
                  and (repetitoire.nom_repetitoire = 'Algorithmes et programme');";
        $result1 = mysqli_query($conn, $sql1);

        // Title :
        echo "<br>";
        echo "<h2>Sujets abordés</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>Nom des Sujets</th>
                </tr>";
        while ($row = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>" . $row['nom_sujet'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 2 :
        $sql2 = "select etudiant.*, inscrire.payer
                    from etudiant,
                         inscrire,
                         repetitoire
                    where inscrire.ID_repetitoire = repetitoire.ID_repetitoire
                      and etudiant.email = inscrire.email_etudiant
                      and nom_repetitoire = 'Algorithmes et programme';";
        $result2 = mysqli_query($conn, $sql2);

        // Title :
        echo "<br>";
        echo "<h2>Liste d'étudiants inscrits</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                    <th>Répétitoire payé</th>
                    
                </tr>";
        while ($row = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "<td>" . $row['payer'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // SQL 3 :
        $sql3 = "select distinct repetiteur.*
                    from repetiteur,
                         repetitoire,
                         donner
                    where repetiteur.email = donner.email_repetiteur
                      and nom_repetitoire = 'Algorithmes et programme'
                      and repetitoire.ID_repetitoire = donner.ID_repetitoire;";
        $result3 = mysqli_query($conn, $sql3);

        // Title :
        echo "<br>";
        echo "<h2>Liste des répétiteurs</h2>";
        echo "<br>";

        // Generates rows :
        echo "<table>
                <tr>
                    <th>e-mail</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Faculté</th>
                    <th>Niveau d'études</th>
                </tr>";
        while ($row = mysqli_fetch_array($result3)) {
            echo "<tr>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['faculte'] . "</td>";
            echo "<td>" . $row['niveau_etude'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";

        // Close connection
        mysqli_close($conn);
        break;

    default :
        echo "HESS !!!!!";
}

?>
</body>
</html>
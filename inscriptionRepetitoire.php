<?php
require_once "server/dbConnection.php";

$nom_repetitoire = $_REQUEST['nom_repetitoire'];
$date_heure = $_REQUEST['date_heure'];
$duree = $_REQUEST['duree'];
$nom_sujet_1 = $_REQUEST['nom_sujet_1'];
$nom_sujet_2 = $_REQUEST['nom_sujet_2'];
$nom_sujet_3 = $_REQUEST['nom_sujet_3'];
$nom_sujet_4 = $_REQUEST['nom_sujet_4'];
$nom_sujet_5 = $_REQUEST['nom_sujet_5'];
$email = $_REQUEST['email'];
$prix_repetitoire_heure = $_REQUEST['prix_repetitoire_heure'];

$date = date('Y/m/d', strtotime($date_heure));
$sujets = array($nom_sujet_1, $nom_sujet_2, $nom_sujet_3, $nom_sujet_4, $nom_sujet_5);


// Sends first SQL Query
$SQLQueryRepetitoire = "insert into repetitoire(nom_repetitoire, date_heure, duree, prix_repetitoire_heure)
values (?,?,?,?);";
$stmt = $conn->prepare($SQLQueryRepetitoire);
$stmt->bind_param("ssii", $nom_repetitoire, $date, $duree, $prix_repetitoire_heure);
$stmt->execute();
$stmt->close();

// Sends second SQL Query with a loop for the differents sujets
$SQLQuerySujet = "insert into sujet(nom_sujet)
VALUES (?)";
for ($index = 0; $index < count($sujets); $index++) {
    if ($sujets[$index] != "") {
        $stmt = $conn->prepare($SQLQuerySujet);
        $stmt->bind_param("s", $sujets[$index]);
        $stmt->execute();
        $stmt->close();
    }
}

// Gets the ID_repetitoire
$SQLIDRepetitoire = "select ID_repetitoire from repetitoire where nom_repetitoire=?";
$stmt = $conn->prepare($SQLIDRepetitoire);
$stmt->bind_param("s", $nom_repetitoire);
$stmt->execute();
$tempID = $stmt->get_result();
$ID_repetitoire= $tempID->fetch_assoc();
//echo "<h2>" . $ID_repetitoire . "</h2>";
//printf("%%d = '%d'\n", $ID_repetitoire);
$stmt->close();

// Sends third SQL Query
$SQLQueryDonner = "insert into donner(EMAIL_REPETITEUR, ID_REPETITOIRE) 
VALUES (?,?);";
$stmt = $conn->prepare($SQLQueryDonner);
$stmt->bind_param("si", $email, $ID_repetitoire);
$stmt->execute();
$stmt->close();

// Gets the ID_sujet and stores them in an array
$SQLIDSujet = "select ID_sujet from sujet where nom_sujet=?";
$ID_sujets = array();
for ($index = 0; $index < count($sujets); $index++) {
    if ($sujets[$index] != "") {
        $stmt = $conn->prepare($SQLIDSujet);
        $stmt->bind_param("s", $sujets[$index]);
        $stmt->execute();
        $temp = $stmt->get_result();
        $ID_sujets[$index] = $temp->fetch_assoc();
        $stmt->close();
    }
}

// Sends forth SQL Query with a loop for the differents sujets
$SQLQueryCreerRepetitoire = "insert into creer_repetitoire(ID_repetitoire, ID_sujet, email_repetiteur)
VALUES (?,?,?);";
for ($index = 0; $index < count($ID_sujets); $index++) {
    $stmt = $conn->prepare($SQLQueryCreerRepetitoire);
    $stmt->bind_param("iis", $ID_repetitoire, $ID_sujets[$index], $email);
    $stmt->execute();
    $stmt->close();
}


// Sends fifth SQL Query with a loop for the differents sujets
$SQLQueryCreerSujet = "insert into creer_sujet(ID_sujet, email_repetiteur) 
VALUES (?,?);";
for ($index = 0; $index < count($ID_sujets); $index++) {
    $stmt = $conn->prepare($SQLQueryCreerSujet);
    $stmt->bind_param("is", $ID_sujets[$index], $email);
    $stmt->execute();
    $stmt->close();
}

// Ends connection and send to confirmation page
header("location: confirmationInscriptionRepetitoire.html");
$conn->close();
